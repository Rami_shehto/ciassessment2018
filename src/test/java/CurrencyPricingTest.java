

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class CurrencyPricingTest
{

    
    CurrencyPricing test;
    
    
    @Before 
    public void setUp ()
    {
        test = new CurrencyPricing();
    }
    
    
    @Test 
    public void testMinorCurrency ()
        throws Exception
    {
        
        
        
        assertEquals (1.021125, test.getFXQuote ("AED", "SAR"), 0.0001);
        
    }
    
    @Test 
    public void testMajorCurrency ()
        throws Exception
    {
        
        
        
        assertEquals (3.75, test.getFXQuote ("USD", "SAR"), 0.0001);
    }
    
}
